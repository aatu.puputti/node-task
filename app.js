const express = require('express');
const app = express();

/**
 * Add two numbers together
 * @param {number} a first param
 * @param {number} b second param
 * @returns {number} sum of a and b
 */
const add = (a,b) => {
    return a + b;
}

app.get('', (req, res) => {
    const sum = add(1,2);
    console.log(sum);
    res.send('ok')
});

app.listen(3000, () => {
    console.log('Server listening on localhost:3000');
});